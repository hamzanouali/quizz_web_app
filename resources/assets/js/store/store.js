require("babel-polyfill")
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store = new Vuex.Store({

  strict : true,

  state: {
    fromState_isAuth : '',
    pageRequireAuth: '',
    gapi: '',
    last_route: '/',
    remember_me_list : 'empty',
    sideModal: false,
    open_remember_modal: false,
    categories : [],
    categories_in_dropdown : [],
    arr_titles_remember_me_list : []
  },

  getters :{
    fromState_isAuth : state => {
      return state.fromState_isAuth;
    },
    pageRequireAuth: state => {
      return state.pageRequireAuth;
    },
    gapi: state => {
      return state.gapi;
    },
    last_route: state => {
      return state.last_route;
    },
    remember_me_list: state => {
      return state.remember_me_list;
    },
    arr_titles_remember_me_list: state => {
      return state.arr_titles_remember_me_list;
    },
    sideModal: state => {
      return state.sideModal;
    },
    open_remember_modal: state => {
      return state.open_remember_modal;
    },
    categories: state => {
      return state.categories;
    },
    categories_in_dropdown: state => {
      return state.categories_in_dropdown;
    }
  },

  mutations: {
    fromState_isAuth: (state,bool_val) =>{
      state.fromState_isAuth = bool_val;
    },
    pageRequireAuth : (state,bool_val) => {
      state.pageRequireAuth = bool_val;
    },
    gapi : (state,object) => {
      state.gapi = object;
    },
    last_route: (state,string) => {
      state.last_route = string;
    },
    remember_me_list: (state,object) => {
      state.remember_me_list.push(object);
    },
    arr_titles_remember_me_list : (state,string) => {
      state.arr_titles_remember_me_list.push(string);
    },
    remove_remember_me_list: state => {
      state.remember_me_list = [];
    },
    sideModal: (state,bool_val) => {
      state.sideModal = bool_val;
    },
    open_remember_modal: (state,bool_val) => {
      state.open_remember_modal = bool_val;
    },
    categories: (state,arr) => {
      state.categories = arr;
    },
    categories_in_dropdown: (state,arr) => {
      state.categories_in_dropdown = arr;
    }
  }

});
