require('./bootstrap.js')
import router from './routes.js'
import { store } from './store/store'
require('./components.js');

/*
------------------------------------------------------------------------------------------
here i am catching all the 'matas' from the current route that i am trying to enter it now
and then do what should i do.
for example: if the route require from me to be logged in, i will check if i am logged in
or i will redirect myself to the login page.
------------------------------------------------------------------------------------------
*/
router.beforeEach( (to,from,next)=>{

  // if(to.matched.length == 0) return next("/404_route");

  if(to.path != '/login' && to.path != "/register" && to.path != "/logout"){
    store.commit('last_route',to.path);
  }
  if(to.meta.isAuth == true){
    store.commit('pageRequireAuth',true);
    if(store.getters.fromState_isAuth == ''){
      axios.get('/isAuth')
      .then(response => {
        console.log(' please wait, we are redirecting you now.. ');
        store.commit('fromState_isAuth',response.data);
        if(store.getters.fromState_isAuth == false){
          return next('/login');
        }else if(store.getters.fromState_isAuth == true){
          return next();
        }else if(store.getters.fromState_isAuth == ''){
          console.log(' no yet baby.. ');
        }

      })
    }else if(store.getters.fromState_isAuth == false){
      return next('/login');
    }else if(store.getters.fromState_isAuth == true){
      return next();
    }
  }else{
    if(to.path == '/login' || to.path == '/register'){
      if(store.getters.fromState_isAuth == true){
        return next('/whoops');
      }else{
        let imported = document.createElement('script');
        imported.src = 'https://apis.google.com/js/platform.js';
        document.head.appendChild(imported);

        return next();
      }
    }else{
      return next();
    }
  }
} );

new Vue({
  beforeCreate(){
    NProgress.start();
    if(store.getters.fromState_isAuth == ''){
      axios.get('/isAuth')
      .then(res => {
        store.commit('fromState_isAuth',res.data);
      });
    }
  },
  mounted(){
    NProgress.done();
    document.write(' okey ');
  },
  el : '#app',
  router,
  store
});
