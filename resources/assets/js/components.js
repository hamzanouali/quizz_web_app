

Vue.component('timer',{

  props: ['minutes','seconds','runIt','timeReady'],

  template : `
    <div v-if="timeReady" :class="clock_class">
      <span>
        <i class="fa fa-clock-o m-t-9"></i>  {{ min | twoDigit }}
      </span>
      <span >:</span>
      <span>{{ sec | twoDigit }}</span>
    </div>
  `,
  data(){
    return {
      clock_class : 'general_clock green_clock',
      min : this.minutes,
      sec : this.seconds,
      interval : '',
    }
  },

  filters: {
    twoDigit : value => {
        if(value % 10 == value){
            return '0'+value;
        }else{
            return value;
        }
    }
  },

  watch : {
    runIt : function(value){
      if(value && this.timeReady){
        this.start();
      }else if(value == false){
        clearInterval(this.interval);
      }
    }
  },

  methods: {

    start(){

        let interval = setInterval( ()=>{


            if(this.sec <= 30 && this.sec > 10 && this.min == 0){
              this.clock_class = "general_clock yellow_clock";
            }

            if(this.sec <= 10 && this.min == 0){
              this.clock_class = "general_clock";
            }

            if(this.sec != 0){
                this.sec--;
            }else{
                if(this.min != 0){
                    this.min--;
                    this.sec = 59;
                }else{
                  this.$emit('time-out',true);
                    clearInterval(interval);
                }
            }
        },1000);
        this.interval = interval;

    }

  }
});

// fixed navbar and fixed side bar
Vue.component('nav-component',{
    template : `
    <div>


      <div ref="navbar" class="columns my-navbar-container">

        <div class="column my-left-side-nav">
          <ul>
            <li>
              <a @click="openSideModal" class="left-first navbar-item fa fa-list"> </a>
            </li>
          </ul>
        </div>

        <div class="column is-10 my-right-side-nav">
          <ul>
            <li>
              <router-link to="/">
                <a class="first navbar-item fa fa-home"></a>
              </router-link>
            </li>
            <li v-for="cat in $store.getters.categories" class="is-hidden-mobile">
                  <router-link :to="{ name: 'category', params: {category:cat.category} }">
                      <a class="first navbar-item">{{ cat.category }}</a>
                  </router-link>
            </li>

            <li class="is-relative is-hidden-mobile">
              <a @click="openDropDwon" v-if="$store.getters.categories_in_dropdown.length>0" class="first navbar-item"> المزيد.. </a>
              <div v-if="dropdown" class="dropdown-more">
                <ul>
                  <li v-for="cat in $store.getters.categories_in_dropdown">
                    <a class="first navbar-item">
                      <router-link :to="{ name: 'category', params: {category:cat.category} }">
                          {{ cat.category }}
                      </router-link>
                    </a>
                  </li>
                </ul>
              </div>
            </li>

            <li @click="modal = true" class="is-hidden-tablet is-hidden-desktop">
              <a class="first navbar-item">تصنيفات</a>
            </li>

          </ul>
        </div>

      </div>

      <div v-if="modal" class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-content">

        <aside class="menu">
          <p class="menu-label">
            التصنيفات
          </p>
          <ul class="menu-list">
            <li v-for="cat in $store.getters.categories">
              <router-link :to="{ name: 'category', params: {category:cat.category} }">
                  {{ cat.category }}
              </router-link>
            </li>
          </ul>
        </aside>


        </div>
        <button @click="modal = false" class="modal-close is-large" aria-label="close"></button>
      </div>


      <!--

      #/_ DESKTOP NAVBAR ************************

      ---->
      <nav class="navbar is-transparent is-hidden">

          <div id="navMenuTransparentExample" class="navbar-menu">

            <div class="navbar-start">
              <a class="navbar-item" @click="openSideModal">
                <i class="fa fa-list"></i>
              </a>
            </div>

            <div class="navbar-end txt-right">

              <div class="navbar-item has-dropdown is-hoverable">
                <a v-if="$store.getters.categories_in_dropdown.length > 0" class="navbar-item ">
                  المزيد..
                </a>
                <div class="navbar-dropdown is-boxed">

                  <span class="navbar-item" v-for="cat in $store.getters.categories_in_dropdown">
                  <router-link :to="{ name: 'category', params: {category:cat.category} }">
                      {{ cat.category }}
                  </router-link>
                  </span>

                </div>
              </div>
              <li v-for="cat in $store.getters.categories">
                    <router-link :to="{ name: 'category', params: {category:cat.category} }">
                        <a class="first navbar-item">{{ cat.category }}</a>
                    </router-link>
              </li>

            </div>

          </div>
        </nav>


      <!--

      #/_ SIDE MODAL ****************************

      ---->
      <div v-if="$store.getters.sideModal">
        <div class="columns sideHiddenDiv is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile divOverFlow">

            <p @click="$store.commit('open_remember_modal',true)" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon fa fa-clock-o">
                <span :class="counted_class" v-text="$store.getters.remember_me_list.length"></span>
              </span>
              <span class="column is-12 the-text"> الإجتياز لاحقا </span>
            </p>

            <p class="columns is-multiline sideICON">
              <span class="column is-12 the-icon fa fa-cog"></span>
              <span class="column is-12 the-text"> إعداداتي </span>
            </p>

            <p class="columns is-multiline sideICON">
              <span class="column is-12 the-icon fa fa-info-circle"></span>
              <span class="column is-12 the-text"> إرشادات </span>
            </p>

            <p @click="$router.push('/logout')" class="columns is-multiline sideICON">
              <span class="column is-12 the-icon fa fa-sign-out"></span>
              <span class="column is-12 the-text"> خروج </span>
            </p>

          </div>

          <div class="column" @click="closeModal" style="background:none">
          </div>
        </div>

        <div v-if="$store.getters.fromState_isAuth == false" class="columns sideHiddenDiv second-one is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile">

            <!-- sign-in button -->
            <span @click="$router.push('/login')" class="button is-default">
              <span> تسجيل الدخول </span>
              <span class="fa fa-envelope-o"></span>
            </span>
            <!-- create account -->
            <span @click="$router.push('/register')" class="button is-default">
              <span> إنشاء حساب </span>
              <span class="fa fa-envelope-o fa-handshake-o"></span>
            </span>

            <p> أو يمكنك إستخدام حسابك في </p>

            <!--Google sign-in button -->
            <span class="button is-default">
              <span> Google </span>
              <span class="fa fa-envelope-o fa-google"></span>
            </span>

            <!--Facebook sign-in button -->
            <span class="button is-default is-info">
              <span> Facebook </span>
              <span class="fa fa-envelope-o fa-facebook"></span>
            </span>

          </div>

          <div class="column" @click="$store.commit('sideModal',false)" style="background:none">
          </div>
        </div>

        <div v-if="$store.getters.open_remember_modal && $store.getters.fromState_isAuth" class="columns sideHiddenDiv second-one third-one is-mobile">
          <div class="column is-2-desktop is-one-quarter-tablet is-half-mobile contDiv">

            <!-- deleting button -->
            <span v-if="!remove_remember_me_list_in_progress" @click="deleteRememberList" id="removeRememberList" class="button is-default">
              <span> حذف الكل </span>
              <span class="fa fa-envelope-o fa-trash-o"></span>
            </span>

            <!-- deleting button in progress -->
            <span v-if="remove_remember_me_list_in_progress" id="removeRememberList" class="button is-loading is-default">
            </span>

            <!-- -->
            <div v-for="elem in $store.getters.remember_me_list" class="eachTitle">
              <router-link :to="{ name : 'show', params: { id: elem.id , title: elem.title } }">
                  {{ elem.title }}
              </router-link>
            </div>

          </div>

          <div class="column" @click="$store.commit('open_remember_modal',false)" style="background:none">
          </div>
        </div>

      </div>
    </div>
    `,
    data() {
      return {
        sideModal : false,
        open_remember_modal : false,
        counted_value : 0,
        counted_class : 'button is-primary counted-btn',
        categories : [],
        categories_in_dropdown : [],
        dropdown : false,
        modal : false,
        remove_remember_me_list_in_progress : false
      }
    },

    mounted(){
      if(this.$store.getters.categories.length == 0) this.getOldCategories();

      // window.onscroll = ()=>{
      //   if(document.documentElement.scrollTop >= 300){
      //     console.log(this.$refs.navbar.setAttribute('class','new-nav-style columns my-navbar-container'));
      //   }
      // }
    },

    methods: {

      catModal(){
        if(this.modal){
          this.modal = false;
        }else{
          this.modal = true;
        }
      },

      toByCategoryPage(category){
        console.log('goion');
        this.$router.push('/by_category/'+category);
      },

      openDropDwon(){
        if(!this.dropdown){
          this.dropdown = true;
        }else{
          this.dropdown = false;
        }
      },

      closeModal(){
        this.$store.commit('open_remember_modal',false);
        this.$store.commit('sideModal',false);
      },

      openSideModal(){
        this.$store.commit('sideModal',true);
        if(this.$store.getters.remember_me_list == 'empty'){
          this.counted_class = 'button is-primary counted-btn is-loading';

          // get remember list from database
          axios.get('/getRememberList')
            .then(res => {
              this.counted_class = 'button is-primary counted-btn';
              this.$store.commit('remove_remember_me_list');
              this.$store.commit('remove_remember_me_list');
              res.data.forEach(element => {
                this.$store.commit('remember_me_list',{ title : element.title, id: element.id } );
              });
              this.counted_value = this.$store.getters.remember_me_list.length;
            })
            .catch(err => console.log(err.data));
        }
      },

      deleteRememberList(){
        this.remove_remember_me_list_in_progress = true;
        axios.get('/deleteRememberList')
          .then(res => {

              this.remove_remember_me_list_in_progress = false;
              this.$store.commit('remove_remember_me_list');
              this.counted_value = 0;

          })
          .catch(err => {
            console.log(err.data);
          })
      },

      getOldCategories(){
        axios.get('/get_old_categories')
          .then(res => {
              if(res.status == 200){

                // this.$store.commit('categories',res.data);
                let categories_in_dropdown = res.data;

                if(res.data.length > 8){
                  this.$store.commit('categories',categories_in_dropdown.slice(0,9));
                  this.$store.commit('categories_in_dropdown',res.data.slice(9,res.data.length));
                }

              }
          })
          .catch(err => {
              console.log(err);
        });
      }

    }
  });
