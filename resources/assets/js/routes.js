import VueRouter from 'vue-router';
import axios from 'axios';
import { store } from './store/store';
import apiHelpers from './api_helpers';

let routes = [
  {
    path : '/',
    component : require('./views/home')
  },

  {
    path : '/404_route',
    component : {
      template : '<p>404 page not f</p>'
    }

  },

  {
    path : '/by_category/:category',
    name : 'category',
    component : require('./views/by_category')

  },

  {
    path : '/about',
    component : require('./views/about')
  },

  {
    path : '/register',
    component : require('./views/auth/register')
  },

  {
    path : '/login',
    component : require('./views/auth/login')
  },

  {
    path : '/whoops',
    component : {
      template : `
        <div style="font-size:25px;font-weight:200;text-align:center;background:none;padding:50px;">
          <p style="padding-bottom:25px;border-bottom:1px dashed #ccc">ما الذي تحاول فعله !</p>
          <router-link style="font-size:20px;" to="/home"> الحساب الشخصي </router-link> -
          <router-link style="font-size:20px;" to="/"> الصفحة الرئيسية </router-link>
        </div>

      `
    }
  },

  {
    path : '/home',
    component : require('./views/dashboard'),
    meta : {
      isAuth : true
    }
  },

  {
    path: '/post',
    component : require('./views/user/post'),
    meta : {
      isAuth: false
    }
  },

  {
    path: '/choose_post',
    component : require('./views/user/choose_post'),
    meta : {
      isAuth: false
    }
  },

  {
    path: '/show/:id/:title',
    name : 'show',
    component : require('./views/show')
  },

  {
    path: 'post/show/:id/:title',
    component : require('./views/user/show_for_result_cases')
  },

  {
    path: '/logout',
    beforeEnter : (to,from,next)=> {
      axios.get('/logout')
        .then(res => {
          if(res.data == false){
            store.commit('fromState_isAuth',res.data);
            apiHelpers.getFirebase();
            firebase.auth().signOut().then( res => {
              return next(store.getters.last_route);
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  },

  {
    path : '/create_tags',
    component : require('./views/user/create')
  },

  {
    path : '/create_categories',
    component : require('./views/user/create_cat')
  },

  {
    path : '/search=:string?',
    component : require('./views/search')
  },

  {
    path : '/update_exams',
    component : require('./views/user/update_exams')
  },

  {
    path : '/update_tmp_exam/:id?',
    name : 'update_tmp_exam',
    component : require('./views/user/post')
    //component : require('./views/user/update')
  },

  {
    path : '/update_exam/:id?/:title?',
    name : 'update_exam',
    component : require('./views/user/post')
    //component : require('./views/user/update')
  }

];

export default new VueRouter({
  routes
})
