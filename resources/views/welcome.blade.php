<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="google-signin-client_id" content="697146948664-b2ouoisre6uminb75fh9sftbs2hql9ui.apps.googleusercontent.com">
        <title> firstapp </title>

        <link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/2.4.0/firebase-ui-auth.css" />

    </head>
    <body>

      <div id="app">
        <router-view></router-view>
      </div>
      <div id="end"></div>
      <script src="{!! asset('js/app.js') !!}"></script>
      <link rel="stylesheet" href="css/app.css">

    </body>
</html>
