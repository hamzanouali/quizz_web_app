<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CommentFormRequest;

use App\Comment;

class CommentsController extends Controller
{
    public function create($title,CommentFormRequest $request){

        $comments = new Comment([
            'post_id' => $request->post_id,
            'content' => $request->comment,
            'user_id' => $request->user_id
        ]);
        $comments->save();
        return redirect()->back()->with('status','Your comment has been posted successfully.');
    }

}
