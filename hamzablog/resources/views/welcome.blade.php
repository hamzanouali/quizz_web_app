@extends('master')

@section('title','Welcome!')

@section('content')

    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                    @endauth
            </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                Laravel
            </div>

            <div class="links">
                <a href="{!! action('PostsController@create') !!}">New Post</a>
                <a href="https://laracasts.com">All Posts</a>
                <a href="#">Admin</a>
                <a href="https://forge.laravel.com">#</a>
                <a href="https://github.com/laravel/laravel">#</a>
            </div>
        </div>
    </div>
@endsection

