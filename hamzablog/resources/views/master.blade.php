<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> @yield('title') </title>

    <link href="{!! asset('Lato/fonts/latofonts.css') !!}" type="text/css" rel="stylesheet"/>
    <link href="{!! asset('bootstrap-3.3.7-dist/css/bootstrap.min.css') !!}" type="text/css" rel="stylesheet"/>
    <link href="{!! asset('css/style.css') !!}" type="text/css" rel="stylesheet"/>

</head>
<body>

@include('shared.navbar')

@yield('content')

<script src="{!! asset('js/jquery-3.2.1.min.js') !!}"></script>
<script src="{!! asset('bootstrap-3.3.7-dist/js/bootstrap.js') !!}"></script>
<script src="{!! asset('bootstrap-3.3.7-dist/js/npm.js') !!}"></script>
</body>
</html>