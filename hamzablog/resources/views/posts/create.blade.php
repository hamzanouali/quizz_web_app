@extends('master')

@section('title','New post')

@section('content')
    <br><br><br>
<div class="container">

    <div class="panel panel-default col-md-12">
        <br><br>
        @if(session('status'))
            <div class="alert alert-success">
                {{  session('status')  }}
            </div>
        @endif

        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                {{  $error  }}
            </div>
        @endforeach

        <form class="form col-md-12" method="post">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">

            <div class="form-group">
                <label> title: </label>
                <input type="text" name="title" class="form-control" placeholder="title example">
            </div>

            <div class="form-group">
                <label> content: </label>
                <textarea name="text" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <span class="btn btn-default"><a href="/"> Cancel </a></span>
                <button class="btn btn-success"> Submit </button>
            </div>

        </form>
        <br><br>
    </div>

    <br>
    <h1> #All posts: </h1>
    <hr>
    <br>

    @foreach($allposts as $post)
        <div class="panel panel-default col-md-12">
            <div class="col-md-12">

                <a href="{!! route('show_post',['title'=>str_replace(' ','-',$post->title)]) !!}">
                    <h1>
                        <b>{{  $post->title  }}</b>
                    </h1>
                </a>
                <hr>
                <p>{{  $post->content  }}</p>

            </div>
        </div>
    @endforeach

</div>
@endsection








