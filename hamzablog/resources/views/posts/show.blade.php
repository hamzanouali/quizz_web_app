@extends('master')

@section('title','New post')

@section('content')

    <br>
    <br>
    <br>
    <div class="container">

        <div class="panel panel-default col-md-12">

            <h1><b> {{  $post->title  }} </b></h1>
            <hr>
            <p>
                {{  $post->content  }}
            </p>
        </div>

    </div>

    @include('posts.comments')

@endsection
