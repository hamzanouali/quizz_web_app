<br>
<div class="container">

    <h1> #Comments:  </h1>
    <hr>

    @include('posts.newcomment')
    <br>
    @if($comments->isEmpty())
        <span> No comment has been posted yet, be the first one. </span>
    @else
        @foreach($comments as $comment)
            <div class="panel panel-default">
                <p> {{  $comment->content  }} </h3>
            </div>
            <br>
        @endforeach
    @endif

</div>