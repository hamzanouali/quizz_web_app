<div class="panel panel-default col-md-12">
    <br><br>
    @if(session('status'))
        <div class="alert alert-success">
            {{  session('status')  }}
        </div>
    @endif

    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            {{  $error  }}
        </div>
    @endforeach

    <form class="form col-md-12" method="post" action="{!! action('CommentsController@create',['title'=>$post->title]) !!}">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" name="user_id" value="0"/>
        <input type="hidden" name="post_id" value="{{  $post->id  }}">

        <div class="form-group">
            <label> comment: </label>
            <textarea name="comment" class="form-control"></textarea>
        </div>

        @if(Auth::check())
            <div class="form-group">
                <button class="btn btn-success"> Comment </button>
            </div>
        @else
            <div class="form-group">
                <a class="btn btn-success" href="{{  route('login')  }}"> login </a>
            </div>
        @endif
    </form>
    <br><br>
</div>