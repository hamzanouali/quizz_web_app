<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

    Route::get('show/{title}','PostsController@show')->where([
        'title' => '[a-zA-Z\p{Arabic}0-9\-!]+'
    ])->name('show_post');

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/send','MailController@send');

    Route::group(['middleware'=>['auth','revalidate']],function(){

        Route::post('show/{title}','CommentsController@create')->where([
            'title' => '[a-zA-Z\p{Arabic}0-9\-!]+'
        ]);

        Route::get('/newpost','PostsController@create');
        Route::post('/newpost','PostsController@store');
        Route::get('/home', 'HomeController@index');

    });

