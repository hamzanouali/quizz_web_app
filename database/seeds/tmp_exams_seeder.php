<?php

use Illuminate\Database\Seeder;

class tmp_exams_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $i = 0;
      for($i ; $i< 45 ; $i++){
        \DB::table('tmp_exams')->insert([
          'title' => str_random(191),
          'question' => str_random(191),
          'answer' => str_random(500),
          'timer' => 50,
          'category' => str_random(10),
          'tags' => str_random(20),
          'background' => ' '
        ]);
      }
    }
}
