<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home',function(){
  return 'home';
});
/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : HOME PAGE ROUTES
  -------------*/

  Route::get('/recentExams/{count}',function($count){
      $data = \DB::table('exams')->latest()->skip($count)->take(3)->get(['id','title','category','background']);
      return json_encode($data);
  });

  Route::get('/category/{category}/{count}',function($category,$count){
      $data = \DB::table('exams')->latest()->whereCategory($category)->skip($count)->take(3)->get(['id','title','category','background']);
      return json_encode($data);
  });

  Route::get('/exams_where_category/{category}/{count}',function($category,$count){
      $data = \DB::table('exams')->latest()->whereCategory($category)->take(1)->skip($count)->get(['id','title','category','background']);
      return json_encode($data);
  });

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : AUTH ROUTES
  -------------*/

  Auth::routes();

  Route::get('/isAuth',function(){
      return \Auth::check() ? 'true' : 'false';
  });

  Route::get('/logout',function(){
    \Auth::logout();
    return \Auth::check() ? 'true' : 'false';
  });

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : CREATE (POST PAGE) EXAM ROUTES
  -------------*/

  Route::post('/createExam','ExamController@create');

  Route::post('/saveTmpExam','ExamController@updateTmp');

  Route::get('/get_tmp_exams_existance','ExamController@getTmpExamsExistance');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : ALL UPDATE EXAM ROUTES
  -------------*/
  Route::get('/get_tmp_exams_for_update/{count}',function($count){
    $result = \DB::table('tmp_exams')->latest()->skip($count)->take(15)->get(['id','title']);
    return json_encode($result);
  });

  Route::get('/get_exams_for_update/{count}',function($count){
    $result = \DB::table('exams')->latest()->skip($count)->take(15)->get(['id','title']);
    return json_encode($result);
  });

  Route::get('/get_tmp_exam/{id}',function($id){
    $result = \DB::table('tmp_exams')->whereId($id)->get();
    return json_encode($result);
  });

  Route::get('/get_exam/{id}',function($id){
    $result = \DB::table('tmp_exams')->whereId($id)->get();
    return json_encode($result);
  });

  Route::post('/remove_tmp_exam','ExamController@destroyTmp');

  Route::post('/remove_exam','ExamController@destroy');

  Route::post('/update_exam','ExamController@update');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : TAGS ROUTES
  -------------*/
  Route::get('/get_old_tags',function(){
      $data = \DB::table('tags')->get(['tag']);
      return json_decode($data);
  });

  Route::post('save_tag','TagsController@create');

  Route::post('removeTag','TagsController@destroy');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : CATEGORIES ROUTES
  -------------*/

  Route::get('/get_old_categories',function(){
      $data = \DB::table('categories')->get(['category']);
      return json_decode($data);
  });

  Route::post('save_category','CategoriesController@create');

  Route::post('removecategory','CategoriesController@destroy');

  /*|
    |
    |
    |
    |
    |
    |
    |
    |
    -------------
    START : REMEMBER EXAMS LIST ROUTES
    -------------*/

    Route::get('/getRememberList',function(){
        $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->get(['exam_id','title']);
        return json_encode($data);
    });

    Route::post('/addToRememberList','RememberListController@create');

    Route::get('/deleteRememberList',function(){
        $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->delete();
        return json_encode($data);
    });

    /*|
      |
      |
      |
      |
      |
      |
      |
      |
      -------------
      START :  SHOW EXAM ROUTES
      -------------*/

    Route::get('/getExam/{id}',function($id){
        $data = \DB::table('exams')->whereId($id)->get();
        return json_encode($data);
    })->where('id','[0-9]+');

    /*|
      |
      |
      |
      |
      |
      |
      |
      |
      -------------
      START :  SEARCH ROUTES
      -------------*/

      Route::post('my_search','ExamController@search');
