<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Exam extends Model
{
    use Searchable;

    protected $table = 'exams';
    protected $fillable = [
      'title',
      'question',
      'answer',
      'timer',
      'category',
      'tags',
      'background',
      'with_prediction',
      'predicted_answers',
      'predicted_messages'
    ];
}
